package week2T;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class UserImpl {

	MagicOfBooks magicOfBooks = new MagicOfBooks();
	Scanner scanner = new Scanner(System.in);

	Week2 week2 = new Week2();

	public void Menu(ArrayList<BookAttributes> arrBook, ArrayList<UserAtributes> arrUser, String Username) {
		// arrUser = ReadFileUser(arrUser);
		System.out.println("Menu");
		System.out.println("1. Display list book");
		System.out.println("2. Display my favorite book ");
		System.out.println("3. Search book by id ");
		System.out.println("4. Sign out ");
		System.out.println("0. Exit ");
		System.out.print("Enter your choice: ");
		int ch = Integer.parseInt(scanner.next());
		System.out.println();
		switch (ch) {
		case 0:
			System.out.println("Stop programing!!!");
			System.exit(0);
			break;
		case 1:
			DisplayListBook(arrBook);
			Menu(arrBook, arrUser, Username);
			break;
		case 2:
			DisplayFavoriteBook(arrBook, arrUser, Username);
			Menu(arrBook, arrUser, Username);
			break;
		case 3:
			SearchBookbyID(arrBook, Username, arrUser);
			Menu(arrBook, arrUser, Username);
			break;
		case 4:
			week2.Login(arrUser, arrBook);
			break;
		default:
			UserImpl tuan1 = new UserImpl();
			System.out.println("Incorrect input format!");
			week2.Login(arrUser, arrBook);
			break;
		}
	}

	public void LogFile(String logMessenger) {
		Logger logger = Logger.getLogger("Log");
		FileHandler fh = null;
		logger.setUseParentHandlers(false);
		try {
			fh = new FileHandler("log.log", true);
			logger.addHandler(fh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);

		logger.info(logMessenger);

	}

	public void DisplayListBook(ArrayList<BookAttributes> arrBook) {

		for (BookAttributes book : arrBook) {
			System.out.println(book.toString());
		}
	}

	public void WriteFile(ArrayList<UserAtributes> arrUser) {
		BufferedWriter bWriter = null;
		try {
			bWriter = new BufferedWriter(new FileWriter("Data.txt"));
			for (int i = 0; i < arrUser.size(); i++) {
				bWriter.write(arrUser.get(i).toStringFile());
				bWriter.newLine();
			}
		} catch (IOException e) {
			// TODO: handle exception
		} finally {
			try {
				bWriter.close();
			} catch (IOException e2) {
				// TODO: handle exception
			}
		}
	}

	public void DisplayFavoriteBook(ArrayList<BookAttributes> arrBook, ArrayList<UserAtributes> arrUser,
			String username) {
		for (UserAtributes user : arrUser) {
			if (user.getUserName().trim().equals(username)) {
				for (int i : user.getFavourite()) {
					for (int j = 0; j < arrBook.size(); j++) {
						if (arrBook.get(j).getBookId() == i) {
							System.out.println(arrBook.get(j).toString());
						}
					}
				}
			}
		}
	}

	public void SearchBookbyID(ArrayList<BookAttributes> arrBook, String username, ArrayList<UserAtributes> arrUser) {
		LogFile("Search");
		System.out.print("Enter ID_book you want to search: ");
		int id = scanner.nextInt();
		System.out.println();
		magicOfBooks.SearchbyID(arrBook, id);
		System.out.print("Do you want to see detail the book? Enter 'Y' to see detail -> ");
		String tmp = scanner.next();
		if (tmp.trim().toUpperCase().equals("Y")) {
			System.out.println();
			DisplayBookByID(arrBook, id);
		}
		System.out.println();
		System.out.print("Do you like it? Enter 'Y' to add book in list favorite book ");
		String tmpString = scanner.next();
		System.out.println();
		if (tmpString.trim().toUpperCase().equals("Y")) {
			for (int a = 0; a < arrUser.size(); a++) {
				if (arrUser.get(a).getUserName().trim().equals(username)) {
					Boolean checkBoolean = true;
					ArrayList<Integer> listtmp = arrUser.get(a).getFavourite();
					for (int i : listtmp) {
						if (id == i) {
							checkBoolean = false;
						}
					}
					if (checkBoolean) {
						arrUser.get(a).getFavourite().add(id);
						System.out.println("Add favourite successful!");
					} else {
						System.out.println("You liked this book before!");
					}
				}
			}
		}
		WriteFile(arrUser);

	}

	public void DisplayBookByID(ArrayList<BookAttributes> arrBook, int id) {
		MagicOfBooks magic = new MagicOfBooks();
		magic.DisplayBookbyID(arrBook, id);
	}
}
