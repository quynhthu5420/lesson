package week2T;

import java.util.ArrayList;
import java.util.Scanner;

public class MagicOfBooks {
	Scanner scanner = new Scanner(System.in);

	public void AddBook(ArrayList<BookAttributes> arrBook) {
		String tmpString = "";
		do {
			int idbook = arrBook.size() + 1;
			System.out.println("BOOK  "+ idbook);
			System.out.println("Enter book's name: ");
			String nameBook = scanner.nextLine();
			System.out.println("Enter book's author: ");
			String Authorname = scanner.nextLine();
			System.out.println("Enter book's description: ");
			String description = scanner.nextLine();
			BookAttributes book = new BookAttributes(idbook, nameBook, Authorname, description);
			arrBook.add(book);
			System.out.println("Enter to exits!");
			tmpString = scanner.nextLine();
		} while (tmpString.trim().equals(""));
	}
	
	public void DisplayBook(ArrayList<BookAttributes> arrBook) {
		for(int i = 0; i< arrBook.size(); i++) {
			System.out.println(arrBook.get(i).toString());
		}
	}
	public void SearchbyID(ArrayList<BookAttributes> arrBook, int id) {
		for(int i = 0; i< arrBook.size(); i++) {
			if (arrBook.get(i).getBookId() == id) {
				System.out.println(arrBook.get(i).getBookId() +" - "+ arrBook.get(i).getBookName());
			}
		}
	}
	public void DisplayBookbyID(ArrayList<BookAttributes> arrBook, int id) {
		for(BookAttributes book : arrBook) {
			if (book.getBookId() == id) {
				System.out.println(book.toString());
			}
		}
	}
	
}

