package week2T;

public class BookAttributes {

	int bookId;
	String bookName, Authorname, description;
	
	public BookAttributes() {
		super();
	}

	public BookAttributes(int bookId, String bookName, String authorname, String description) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.Authorname = authorname;
		this.description = description;
	}
	
	public BookAttributes(String lineString) {
		String [] line = new String[4];
		line = lineString.split(" # ");
		this.bookId = Integer.parseInt(line[0]);
		this.bookName = line[1];
		this.Authorname = line[2];
		this.description = line[3];
	}
	
	public BookAttributes(int bookId) {
		super();
		this.bookId = bookId;
	}


	@Override
	public String toString() {
		return "BookAttributes [bookId=" + bookId + " - bookName=" + bookName + "- Authorname=" + Authorname
				+ " - description=" + description + "]";
	}

	public String getBookName() {
		return bookName;
	}


	public void setBookName(String bookName) {
		this.bookName = bookName;
	}


	public String getAuthorname() {
		return Authorname;
	}


	public void setAuthorname(String authorname) {
		Authorname = authorname;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getBookId() {
		return bookId;
	}


	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	
	
}
