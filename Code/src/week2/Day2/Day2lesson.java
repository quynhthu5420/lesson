package Day2;

import java.util.concurrent.atomic.AtomicInteger;

class even extends Thread {
	@Override
	public void run() {
		for (int i = 1; i <= 20; i++) {
			System.out.println(Thread.currentThread().getName() + ": " + i);
		}
	}

}

class odd extends Thread {

	@Override
	public void run() {
		for (int i = 1; i <= 20; i++) {
			System.out.println(Thread.currentThread().getName() + ": " + i);
		}
	}

}

public class Day2lesson {
	public static void main(String[] args) {
		Thread t1 = new even();
		Thread t2 = new odd();
		t1.start();
		t2.start();
	}
}

