package Day2;
import java.util.Iterator;

public class day2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		String name1 ="guest1";
		String name2 ="guest2";
		String name3 ="guest3";
		Greet t1 = new Greet();
		t1.printGreeting(name1);
		t1.printGreeting(name3);
		t1.printGreeting(name2);
	}

}

class Greet extends Thread  {
	public static void printGreeting(String guestName) throws InterruptedException{
	System.out.println("Welcome "+guestName);
	Greet.sleep(250);
	System.out.println("How are you doing, "+guestName+"?");
	Greet.sleep(250);
	System.out.println("Goodbye for now, see you soon "+guestName);
	Greet.sleep(250);
	}

}


