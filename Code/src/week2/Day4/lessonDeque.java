package Day4;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class lessonDeque {

	public static void main(String[] args) {
		// Declare Deque object
		Deque<String> deque = new LinkedList<String>();
		// add elements to the deque
		deque.add("One"); // add ()
		deque.push("Four"); // push ()
		deque.offer("Five"); // offer ()
		deque.offerFirst("Six"); // offerFirst ()
		deque.offerLast("Seven"); // offerLast ()
		System.out.println("Initial Deque:");

		// insert an element at the head
		deque.addFirst("Two");
		System.out.println("Add first successful!");
		// insert an element at the tail
		deque.addLast("Three");
		System.out.println("Add last successful!");
		// print the deque
		System.out.print(deque + " ");
		// Peek () method
		System.out.println("\nDeque Peek:" + deque.peek());
		// removeAll
		deque.removeAll(deque);
		System.out.println("Delete All Successful!");

	}

}
