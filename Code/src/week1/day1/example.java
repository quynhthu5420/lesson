package day1;

import java.util.Scanner;

public class example {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("My name is Ha Quynh Thu");
		// Input the number of task
		System.out.println("Enter the number of task: ");
		int t = Integer.parseInt(sc.nextLine());
		// Create array
		String[] tak = new String[t];
		// Input String to array
		for (int i = 0; i < tak.length; i++) {
			System.out.print("Task " + (i + 1) + ": ");
			tak[i] = sc.nextLine();
		}

		System.out.println("=============increasing==============");
		// Print array
		int c = 0;
		while (c < tak.length) {
			System.out.println("Task " + (c + 1) + ": " + tak[c]);
			c++;
		}

		System.out.println("==============decreasing=============");
		//
		for (int i = tak.length - 1; i >= 0; i--) {
			System.out.println("Task " + (i + 1) + ": " + tak[i]);
		}
		System.out.println("=============repeated==============");
		for (int i = 0; i < tak.length; i++) {
			boolean check = true;
			int count = 1;
			if (i == 0)
				check = true;
			else {
				for (int k = 0; k < i; k++) {
					if (tak[i].toLowerCase().equals(tak[k].toLowerCase())) {
						check = false;
						break;
					}
				}
			}
			if (check == true) {
				for (int j = i + 1; j < tak.length; j++) {
					if (tak[i].toLowerCase().equals(tak[j].toLowerCase())) {
						count++;
					}
				}
				System.out.println(tak[i] + " " + count + " times");
			}
		}
		for (int i = 0; i < tak.length - 1; i++) {
			for (int j = i + 1; j < tak.length; j++) {
				if (tak[i].compareTo(tak[j]) > 0) {
					String temp = tak[i];
					tak[i] = tak[j];
					tak[j] = temp;
				}

			}
		}

		System.out.println("=============Sort by A-Z==============");
		for (String a : tak) {
			System.out.print(a + "\t");
		}
	}
	}


