package day3;

import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Scanner;
public class Lessonday3 {
	
	private int TaskId;
	private String TaskTitle;
	private String TaskText;
	private String assignedTo;
	public Scanner sc = new Scanner(System.in);
	
	
	public int getTaskId() {
		return TaskId;
	}

	public void setTaskId(int taskId) {
		TaskId = taskId;
	}

	public String getTaskTitle() {
		return TaskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		TaskTitle = taskTitle;
	}

	public String getTaskText() {
		return TaskText;
	}

	public void setTaskText(String taskText) {
		TaskText = taskText;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Lessonday3() {}
	
	public Lessonday3(int taskId, String taskTitle, String taskText, String assignedTo) {
		this.TaskId = taskId;
		this.TaskTitle = taskTitle;
		this.TaskText = taskText;
		this.assignedTo = assignedTo;
	}

	
	
	public String toString(ArrayList<Lessonday3> arrTasks, int i) {
		return "Tasks [TaskId= " + arrTasks.get(i).getTaskId() 
				+ ", TaskTitle= " + arrTasks.get(i).getTaskTitle() 
				+ ", TaskText= " + arrTasks.get(i).getTaskText()  
				+ ", assignedTo= "+ arrTasks.get(i).getAssignedTo()  +  "]";
	}

	public void AddData(ArrayList<Lessonday3> arrTasks) {
		System.out.println("Enter the number of Tasks: ");
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			int id = arrTasks.size();
			System.out.println("Task "+(id+1)+": ");
			System.out.print("Enter the Task Title: ");
			String taskTitle = sc.nextLine();
			System.out.print("Enter the Task Text: ");
			String taskText = sc.nextLine();
			System.out.print("Enter the assigned: ");
			String assignedTo = sc.nextLine();
			Lessonday3 tasks = new Lessonday3(id, taskTitle, taskText, assignedTo);
			arrTasks.add(tasks);
		}
	}
	public int Search(ArrayList<Lessonday3> arrTasks) {
		System.out.println("Enter the Task you want to search: ");
		String search = sc.nextLine();
		int pos = -1;
		for (int i = 0; i < arrTasks.size(); i++) {
			if(arrTasks.get(i).TaskText.toLowerCase().trim().equals(search.toLowerCase().trim())) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	
	public void Update(ArrayList<Lessonday3> arrTasks ) {
		int pos = Search(arrTasks);
		if(pos >= 0) {
			System.out.print("Enter the Task Title: ");
			arrTasks.get(pos).setTaskTitle(sc.nextLine());
			System.out.print("Enter the Task Text: ");
			arrTasks.get(pos).setTaskText(sc.nextLine());
			System.out.print("Enter the assigned: ");
			arrTasks.get(pos).setAssignedTo(sc.nextLine());
			System.out.println("Update Successful");
		}
		else {
			System.out.println("No value exists");
		}
	}
	public void Delete(ArrayList<Lessonday3> arrTasks) {
		int pos = Search(arrTasks);
		if(pos >= 0) {
			Boolean dl;
			System.out.print("Are you sure you want to delete? True / False:  ");
			dl = Boolean.parseBoolean(sc.nextLine());
			if(dl) {
				arrTasks.remove(pos);
				System.out.println("Delete Successful");	
			}
			else {
				System.out.println("Delete Fail");
			}
			
		}
		else {
			System.out.println("No value exists");
		}
	}
	public void DisplaySearch(ArrayList<Lessonday3> arrTasks) {
		int pos = Search(arrTasks);
		if(pos >= 0) {
			System.out.println(toString(arrTasks,pos)+"");
		}
		else {
			System.out.println("No value exists");
		}
	}
	public void Display(ArrayList<Lessonday3> arrTasks) {
		int pos = arrTasks.size();
		if(pos > 0) {
			for(int i = 0; i < arrTasks.size(); i++) {
				System.out.println(toString(arrTasks,i));
			}
		}
		else {
			System.out.println("No value exists");
		}
	}	
}
