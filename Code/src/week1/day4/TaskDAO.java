package day4;

import java.util.ArrayList;

public interface TaskDAO {
public void Input_Arr(ArrayList<Task> arrTasks);
	
	public void DisplayAllTask(ArrayList<Task> arrTasks);
	
	public void DisplayByAssign(String name, ArrayList<Task> arrTasks);

	public void Update_Task(ArrayList<Task> arrTasks);
	
	public void Delete_Task(ArrayList<Task> arrTasks);
	
	public void Search_by_TaskTitle(ArrayList<Task> arrTasks);
	
	public void SortArrTask(ArrayList<Task> arrTasks);
}
