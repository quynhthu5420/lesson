package day4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;


public class Task {
	int TaskID;
	String TaskTitle, TaskText, TaskassignedTo, Taskstatus;
	Date dateText;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	public Task(int taskID, String taskTitle, String taskText, String assignedTo, Date dateText, String status) {
		super();
		TaskID = taskID;
		TaskTitle = taskTitle;
		TaskText = taskText;
		this.TaskassignedTo = assignedTo;
		this.dateText = dateText;
		this.Taskstatus = status;
	}

	public Task() {
		super();
	}

	public void Input_Task(ArrayList<Task> arrTasks) {
		Scanner scanner = new Scanner(System.in);
		TaskID = arrTasks.size() + 1;
		System.out.println("Task " + (TaskID) + ": ");
		System.out.print("Enter the Task Title: ");
		TaskTitle = scanner.nextLine();
		System.out.print("Enter the Task Text: ");
		TaskText = scanner.nextLine();
		System.out.print("Enter the assigned: ");
		TaskassignedTo = scanner.nextLine();
		while (true) {
			System.out.print("Enter the date: ");
			String date = scanner.nextLine();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setLenient(false);
			try {
				setDateText(dateFormat.parse(date));
				if (date.length() == 0) {
					throw new Exception("Date can't null! Please try again!");

				}
				else {
					break;
				}
			} catch (ParseException e) {
				System.out.println("Date have to follow the format day/month/year! Try again!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.print("Enter the Task status: ");
		Taskstatus = scanner.nextLine();
	}

	@Override
	public String toString() {
		return "Task [TaskID=" + TaskID + ", TaskTitle=" + TaskTitle + ", TaskText=" + TaskText + ", TaskassignedTo="
				+ TaskassignedTo + ", Taskstatus=" + Taskstatus + ", dateText=" + dateText + "]";
	}

	public int getTaskID() {
		return TaskID;
	}

	public void setTaskID(int taskID) {
		TaskID = taskID;
	}

	public String getTaskTitle() {
		return TaskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		TaskTitle = taskTitle;
	}

	public String getTaskText() {
		return TaskText;
	}

	public void setTaskText(String taskText) {
		TaskText = taskText;
	}

	public Date getDateText() {
		return dateText;
	}

	public void setDateText(Date dateText) {
		this.dateText = dateText;
	}

	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getTaskassignedTo() {
		return TaskassignedTo;
	}

	public void setTaskassignedTo(String taskassignedTo) {
		TaskassignedTo = taskassignedTo;
	}

	public String getTaskstatus() {
		return Taskstatus;
	}

	public void setTaskstatus(String taskstatus) {
		Taskstatus = taskstatus;
	}

}
