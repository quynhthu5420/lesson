package week1;

import java.util.*;

public class Main {

	public static void Login(ArrayList<User> user, ArrayList<Book> arrBook) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName()) && passString.equals(user.get(i).getPassword())) {
					UserImp userImpl = new UserImp();
					userImpl.Menu(arrBook, user, userString);
				} else {
					tt = false;
				}
			}
			if (tt == false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt == false);
	}

	public static void main(String[] args) {
		ArrayList<Book> arrBookAttributes = new ArrayList<>();
		arrBookAttributes.add(new Book(1, "How to Win Friends and Influence People", "Dale Carnegie",
				"The book gives advice on how to behave, behave and communicate with people to achieve success in life."));
		arrBookAttributes.add(new Book(2, "The Alchemist", "Paulo Coelho",
				"A good book for those who have lost their dream or never had it. If you are looking for books to read for success, The Alchemist is well worth it. How to be successful: success in thought and action."));
		arrBookAttributes.add(new Book(3, "Being A Happy Teenager", "Andrew Matthews",
				"gives readers extremely real situations, even stories that are both \"small\" and \"important\" with wise, interesting and humorous behavior."));

		ArrayList<User> arruserAtributes = new ArrayList<>();
		ArrayList<Integer> arrFavoriteBookArrayList1 = new ArrayList<>();

		arrFavoriteBookArrayList1.add(1);
		arrFavoriteBookArrayList1.add(2);
		arruserAtributes.add(new User(1, "user1", "123", "vu.tu1@hcl.com", arrFavoriteBookArrayList1));

		ArrayList<Integer> arrFavoriteBookArrayList2 = new ArrayList<>();
		arrFavoriteBookArrayList2.add(1);
		arrFavoriteBookArrayList2.add(3);
		arruserAtributes.add(new User(1, "user2", "123", "vu.tu1@hcl.com", arrFavoriteBookArrayList2));

		ArrayList<Integer> arrFavoriteBookArrayList3 = new ArrayList<>();
		arrFavoriteBookArrayList3.add(2);
		arrFavoriteBookArrayList3.add(3);
		arruserAtributes.add(new User(1, "user3", "123", "vu.tu1@hcl.com", arrFavoriteBookArrayList2));

		Login(arruserAtributes, arrBookAttributes);

	}

}
